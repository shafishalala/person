public class Main {
    public static void main(String[] args) {
        Student student = new Student("Shalala Hasanova", 25, "58477");
        student.displayInfo();

        System.out.println();

        Teacher teacher = new Teacher("Lala ", 45, "Java");
        teacher.displayInfo();
    }
}